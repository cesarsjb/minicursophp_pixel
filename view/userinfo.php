<?php
ini_set("display_errors",1);
session_start();

require_once '/home/cesar/projetos/minicursophp_login/control/UserController.php';

$controller = new UserController();

$user = $controller->retrieveUserInfo();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Mini Curso Pixel - User Info</title>
	<link rel="stylesheet" href="">
</head>
<body>

<a href="logout.php">Logout</a>

<br><br>

<h1>Welcome, <?php echo $user["username"];?></h1>

<p>Email: <?php echo $user["email"];?></p>
	
</body>
</html>