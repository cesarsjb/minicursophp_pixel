<?php

require_once 'ConnectionPDO.php';

class UserRepository
{

    private $pdo;

    function __construct()
    {
        $this->pdo = ConnectionPDO::getInstance();
    }

    public function validateUser($username, $password){
        $query = $this->pdo->prepare("SELECT id FROM users WHERE username=:username AND password=:password");
        $query->bindParam(":username", $username);
        $query->bindParam(":password", $password);
        $query->execute();
        $result = $query->fetch();

        return $result["id"];
    }

    public function retrieveUser($idUser){
        $query = $this->pdo->prepare("SELECT * FROM users WHERE id=:id");
        $query->bindParam(":id",$idUser);
        $query->execute();
        return $query->fetch();

    }

}