<?php


class ConnectionPDO
{

    private static $pdo;
    private $db = "mysql";
    private $dbname = "minicursopixel";
    private $host = "localhost";
    private $username = "pixel";
    private $password = "pixel";

    private function __construct()
    {
        try{
            self::$pdo = new PDO("$this->db:dbname=$this->dbname;host=$this->host",$this->username,$this->password);
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public static function getInstance(){
        if(is_null(self::$pdo)){
            new ConnectionPDO();
        }
        return self::$pdo;
    }

}