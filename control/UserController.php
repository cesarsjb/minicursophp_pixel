<?php

require_once '/home/cesar/projetos/minicursophp_login/model/repository/UserRepository.php';

class UserController

{
    private $userRepository;

    function __construct(){
        $this->userRepository = new UserRepository();
    }

    public function login($username,$password){

        $idUser = $this->userRepository->validateUser($username,$password);

        if(!is_null($idUser)){
            $_SESSION["idUser"] = $idUser;
            return true;
        }else{
            return false;
        }            

    }

    public function isLogged(){
        return isset($_SESSION["idUser"]);        
    }

    public function kickUser(){
        header("Location: http://localhost/projetos/minicursophp_login/");
    }

    public function retrieveUserInfo(){
        if($this->isLogged()){
            return $this->userRepository->retrieveUser($_SESSION["idUser"]);
        }
        else{
            $this->kickUser();
        } 
        
    }

    public function logout(){
        
        session_destroy();
        $this->kickUser();
    }
  
    public function invoke(){

        if($_SERVER['REQUEST_METHOD']=="POST"){

            $username = isset($_POST["username"]) ? $_POST["username"] : "";
            $password = isset($_POST["password"]) ? $_POST["password"] : "";

            if($this->login($username, $password)){
                
                header("Location: http://localhost/projetos/minicursophp_login/view/userinfo.php");
            }else{
                return "User or password invalids";
            }
        }
        
    }

    public function verifySession(){
        if($this->isLogged()){
            header("Location: http://localhost/projetos/minicursophp_login/view/userinfo.php");
        }else{
            header("Location: http://localhost/projetos/minicursophp_login/view/login.php");
        }
    }

}